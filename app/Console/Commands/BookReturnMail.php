<?php

namespace App\Console\Commands;

use App\Models\IssuedBook;
use App\Notifications\ReturnBook;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class BookReturnMail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'email:return-book';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send email to user for book return';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $issuedbook = IssuedBook::all();
        foreach ($issuedbook as $issuedbook)
        {
            if(Carbon::now() == $issuedbook->created_at->addDays(13)->toDateString())
            {
                $email = $issuedbook->user;
                $email->notify(new ReturnBook());
            }
        }

    }
}
