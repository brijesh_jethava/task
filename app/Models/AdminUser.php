<?php

namespace App\Models;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

/**
 * @method static whereEmail($get)
 */
class AdminUser extends Authenticatable
{
    use Notifiable;
    protected $fillable = ['username' ,'email','password', 'forgot_password_token'];
}
