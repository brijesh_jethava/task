<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class IssuedBook extends Model
{
    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo(User::class , 'user_id','id');
    }

    public function book()
    {
        return $this->belongsTo(Book::class , 'book_id' , 'id');
    }
}
