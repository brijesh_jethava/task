<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Book;
use App\Models\IssuedBook;
use App\Models\User;
use App\Notifications\ReturnBook;
use Carbon\Carbon;
use Illuminate\Http\Request;

class BookController extends Controller
{
    public function show()
    {
        $book = Book::all();

        return view('user.book' , compact('book'));
    }

    public function issue($id)
    {
        $book = Book::find($id);

        return view('user.issuebook', compact('book'));
    }

    public function postissue($id)
    {
        $book = Book::find($id);
        $book->fill(
            ['quantity' => ($book->quantity) - 1]
        )->save();

        IssuedBook::create([
          'user_id' => auth()->id(),
           'book_id' => $book->id,
            'quantity' => 1,
            'return_date' => Carbon::now()->addDays(15)
        ]);

        session()->flash('success', 'Book ' . $book->book .' issued successfully');

        return redirect(route('show:book'));
    }

    public function userbook($user_id)
    {
        $issuedbook = IssuedBook::with(['book.category', 'user'])->where('user_id' ,$user_id)->get();

        return view('user.mybook' , compact( 'issuedbook'));
    }

    public function bookreturn($id)
    {
        $issuedbook = IssuedBook::find($id);
        $issuedbook->delete();

        $book = Book::find($issuedbook->book_id);
        $book->fill([
            "quantity" => $book->quantity + 1
        ])->save();
        session()->flash('success' ,'Book ' . $book->book. ' returned successfully');

        return redirect(route('user:issued:book' , auth()->id()));
    }
}
