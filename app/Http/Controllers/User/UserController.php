<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\EmailRequest;
use App\Http\Requests\UserLoginRequest;
use App\Http\Requests\UserRequest;
use App\Mail\VerifyMail;
use App\Models\User;

use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Str;

class UserController extends Controller
{

    use AuthenticatesUsers;
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function index()
    {
        if (!Auth::user()) {
            return view('user.index');
        }
        else
        {
            return redirect(route('user:home'));
        }
    }

    public function home()
    {
        if(Auth::user())
        {
            return view('user.home');
        }
        else
        {
            return redirect(route('user:login'));
        }
    }

    public function login()
    {
        return view('user.auth.loginform');
    }

    public function postlogin(UserLoginRequest $request)
    {
            if(Auth::attempt($request->persist()))
            {
                if(auth()->user()->is_verified == 1 && auth()->user()->is_approve == 1)
                {
                        session()->flash('success','Logged in Successfully');
                        return redirect(route('user:home'));
                }
                else
                {
                    session()->flash('error' , 'Kindly verify your email. if you verified then wait for profile approval');
                    return redirect(route('user:login'));
                }
            }
            else
            {
                session()->flash('error' ,'Invalid credentials');
                return redirect(route('user:login'));
            }
    }

    public function logout()
    {
        auth('web')->logout();
        session()->flash('success' , 'Successfully Logged out');

        return redirect()->route('user:login');
    }

    public function signup()
    {
        return view('user.auth.register');
    }

    public function postsignup(UserRequest $request)
    {
         $user = User::create(array_merge($request->persist(),['VerifyEmail_token'=>Str::random(20) . time()]));

         if($user != null)
         {
             VerifyController::sendSignupEmail($user->name, $user->email, $user->VerifyEmail_token);
             session()->flash('success','We have sent an activation link on your email');

             return redirect(route('user:login'));
         }
         else
         {
             session()->flash('error', 'Something is Wrong...!!');

             return redirect(route('user:signup'));
         }
    }

    public function verify()
    {
        $VerifyEmail_token = Request::get('code');
        $user = User::where(['VerifyEmail_token'=>$VerifyEmail_token])->first();

        if($user != null)
        {
            $user->is_verified =  1;
            $user->save();
            session()->flash('success' , 'Your email is successfully verified');

            return redirect(route('user:login'));
        }
        else
        {
            session()->flash('error' , 'Record not found..!');

            return redirect(route('user:login'));
        }
    }
}
