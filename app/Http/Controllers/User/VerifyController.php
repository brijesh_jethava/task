<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\EmailRequest;

use App\Mail\VerifyMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;


class VerifyController extends Controller
{
    public static function sendSignupEmail($name, $email, $VerifyEmail_token)
    {
        $user = [
            'email'=> $email,
            'name' => $name,
            'VerifyEmail_token' => $VerifyEmail_token
        ];
        Mail::to($email)->send(new VerifyMail($user));
    }
}
