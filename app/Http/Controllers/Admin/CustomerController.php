<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;

class CustomerController extends Controller
{
    public function index()
    {
       $result =  User::all();

        return view('admin.registereduser.index' , compact('result'));
    }

    public function approve($id)
    {
        $user = User::where('id',$id)->first();
        $user->is_approve =  true;
        $user->save();
    }

}
