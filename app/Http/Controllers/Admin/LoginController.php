<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    public function getLogin()
    {
        return view('admin.auth.login');
    }

    public function postLogin(LoginRequest $request)
    {

        if (auth('admin')->attempt(['email' => $request->get('email'), 'password' => $request->get('password')])) {
            return redirect()->route('dashboard.index');
        }

        session()->flash('error', 'Invalid Credentials!');

        return redirect()->back()->withInput($request->all());
    }

    public function getLogout()
    {
        auth('admin')->logout();

        return redirect()->route('admin:login');
    }
}
