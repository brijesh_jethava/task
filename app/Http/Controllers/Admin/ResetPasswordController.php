<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\ChangePasswordRequest;
use App\Models\AdminUser;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;

class ResetPasswordController extends Controller
{
    use ResetsPasswords;

        protected $redirectTo = '/admin/login';

        public function __construct()
        {
            $this->middleware('guest');
        }

        public function ResetForm($token)
        {
            $user = AdminUser::where('forgot_password_token' , $token)->firstorfail();

            return view('admin.auth.reset-password', compact('user'));
        }

    public function Reset(ChangePasswordRequest $request, $id)
        {
            $user = AdminUser::findorfail($id);
            $user->fill([
               'password' => bcrypt($request->get('password'))
            ])->save();

            session()->flash('success' , 'password Updated successfully');

            return redirect()->route('admin:login');
        }

}
