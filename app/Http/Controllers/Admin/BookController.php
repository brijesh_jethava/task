<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\BookRequest;
use App\Models\Book;
use App\Models\Category;
use App\Models\IssuedBook;
use Illuminate\Http\Request;

class BookController extends Controller
{
    public function index()
    {
        $book = Book::all();

        return view('admin.Book.book', compact('book'));
    }
    public function create()
    {
        $category = Category::all();

        return view('admin.Book.addbook', compact('category'));
    }

    public function store(BookRequest $request)
    {
        Book::create($request->persist());
        session()->flash('success' , 'Record has been created successfully');

        return redirect(route('book.index'));
    }
    public function edit($id)
    {
        $category = Category::all();
        $book = Book::find($id);

        return view('admin.Book.edit' , compact('book','category'));
    }

    public function update(BookRequest $request,$id)
    {
        $book = Book::find($id);
        $book->fill($request->persist())->save();
        session()->flash('success' , 'Record has been updated successfully');

        return redirect('admin/book');
    }

    public function destroy($id)
    {
        $book =  Book::find($id);
        $book->delete();
        session()->flash('success' , 'Record has been deleted successfully');

        return back();

    }

    public function issuedbook()
    {
        $issuedbook = IssuedBook::all();

        return view('admin.Book.issuedbook' ,compact('issuedbook'));

    }
}
