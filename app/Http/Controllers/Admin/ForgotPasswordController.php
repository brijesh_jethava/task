<?php

namespace App\Http\Controllers\Admin;



use App\Http\Controllers\Controller;
use App\Http\Requests\EmailRequest;
use App\Models\AdminUser;
use App\Notifications\ForgotPasswordMail;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Support\Str;



class ForgotPasswordController extends Controller
{

    use SendsPasswordResetEmails;

    public function __construct()
    {
        $this->middleware('guest');
    }

    public function showLinkRequestForm()
    {
        return view('admin.auth.forgot-password');
    }

    public function sendResetLinkEmail(EmailRequest $request)
    {
        $admin = AdminUser::whereEmail($request->get('email'))->first();
        $token = Str::random(20) . time();
        $admin->fill([
                'forgot_password_token' => $token
        ])->save();

        $admin->notify(new ForgotPasswordMail());
        session()->flash('success', 'Reset Password link sent to your email.');

        return redirect()->route('admin:login');
    }
}
