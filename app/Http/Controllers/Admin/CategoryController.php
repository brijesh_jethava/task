<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\CategoryRequest;
use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function index()
    {
        $category = Category::all();

        return view('admin.category.category' ,compact('category'));
    }

    public function create()
    {
        return view('admin.category.addcategory');
    }

    public function store(CategoryRequest $request)
    {
        Category::create($request->persist());
        session()->flash('success' , 'Record has been created successfully');


        return redirect(route('category.index'));
    }

    public function edit($id)
    {
        $category = Category::find($id);

        return view('admin.category.edit', compact('category'));
    }

    public function update(CategoryRequest $request, $id)
    {
        $category = Category::find($id);
        $category->fill($request->persist())->save();

        return redirect(route('category.index'));
    }
}
