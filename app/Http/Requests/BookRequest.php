<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BookRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'book_name' => 'required',
            'author' => 'required',
            'quantity' => 'required',
            'category_id' => 'required'
        ];
    }

    public function persist()
    {
        return $this->only(
            'book_name','author','quantity','category_id'
        );
    }
}
