<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['namespace'=>'User'] ,function(){
    Route::get('/' , 'UserController@index')->name('user:index');

    Route::get('/login','UserController@login')->name('user:login');
    Route::post('/login','UserController@postlogin')->name('post:user:login');
    Route::get('/logout', 'UserController@logout')->name('user:logout');

    Route::get('register' , 'UserController@signup')->name('user:signup');
    Route::post('register' , 'UserController@postsignup')->name('post:user:register');

    Route::get('/verification' , 'UserController@verify')->name('verify:user');

    Route::get('home' , 'UserController@home' )->name('user:home');
    Route::get('book' , 'BookController@show')->name('show:book');
    Route::get('/book/issue/{id}' , 'BookController@issue')->name('issue:book');
    Route::post('/book/issue/{id}' , 'BookController@postissue')->name('post:issue:book');
    Route::get('mybook/{id}' , 'BookController@userbook')->name('user:issued:book');
    Route::get('return/{id}' , 'BookController@bookreturn')->name('return:book');
});



Route::group(['prefix' => 'admin', 'namespace' => 'Admin'] ,function(){

   Route::group(['middleware' => ['guest:admin']] ,function(){
       Route::get('login' , 'LoginController@getLogin')->name('admin:login');
       Route::post('login', 'LoginController@postLogin')->name('post:admin:login');

        Route::get('password/reset' , 'ForgotPasswordController@showLinkRequestForm')->name('admin:forgot:password');
        Route::post('password/email' , 'ForgotPasswordController@sendResetLinkEmail')->name('post:admin:forgot:password');
        Route::get('password/reset/{token}' , 'ResetPasswordController@ResetForm')->name('admin:reset:password');
        Route::post('password/reset/{id}' , 'ResetPasswordController@Reset')->name('post:admin:reset:password');
   });


Route::group([
    'middleware' => 'auth:admin'
] ,function(){

    Route::get('logout' , 'LoginController@getlogout')->name('admin:logout');

    Route::resource('dashboard' , 'DashboardController');

    Route::resource('customer' , 'CustomerController');

    Route::post('approve/{id}' , 'CustomerController@approve')->name('user:approve');

    Route::resource('book' , 'BookController');

    Route::get('issuedbook' , 'BookController@issuedbook')->name('issued:book');

    Route::resource('category' ,'CategoryController');


});
});
