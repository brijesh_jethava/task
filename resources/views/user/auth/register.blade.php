@extends('user.auth.layout')

@section("title")
    Register
@endsection

@section('content')

    @include('user.auth.messages')

    <form action="{{route('post:user:register')}}" method="post">
        @csrf
        <div  style="margin-top: 10%" class="container shadow py-4 px-4 col-md-4">
            <div class="form-group">
                <label for="name">Name</label>
                <input type="text" name="name" class="form-control" id="name" placeholder="Enter Name">
                @error('name')
                <p style="color: red">{{$message}}</p>
                @enderror
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Email address</label>
                <input type="email" name="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
                @error('email')
                <p style="color: red">{{$message}}</p>
                @enderror
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Password</label>
                <input type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
                @error('password')
                <p style="color: red">{{$message}}</p>
                @enderror
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Confirm Password</label>
                <input type="password" name="confirm_password" class="form-control" id="exampleInputPassword1" placeholder="Password">
                @error('confirm_password')
                <p style="color: red">{{$message}}</p>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary mb-3">Register</button><br/>
            <a href="{{route('user:login')}}">Already have account ?</a>
        </div>
    </form>

@endsection
