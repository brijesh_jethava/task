@extends('user.auth.layout')

@section('title')
    Login
@endsection

@section('content')

    @include('user.auth.messages')

<form action="{{route('post:user:login')}}" method="post" enctype="multipart/form-data">
    @csrf
    <div  style="margin-top: 10%" class="container shadow py-4 px-4 col-md-4">
        <div class="form-group">
            <label for="exampleInputEmail1">Email address</label>
            <input type="email" name="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
            @error('email')
            <p style="color: red">{{$message}}</p>
            @enderror
        </div>
        <div class="form-group">
            <label for="exampleInputPassword1">Password</label>
            <input type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
            @error('password')
            <p style="color: red">{{$message}}</p>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary mb-3">Submit</button><br/>
        <a href="{{route('user:signup')}}">Don't have account ?</a>
        </div>
</form>

@endsection
