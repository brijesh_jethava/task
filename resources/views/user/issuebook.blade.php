@extends('layout.index')

@section('content')
    <form action="{{route('post:issue:book', $book->id)}}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="container bg-light shadow py-4 px-4 col-md-4">
            <div class="form-group">
                <label for="exampleInputEmail1">Name of book</label>
                <input name="book" class="form-control" value="{{$book->book_name}}" disabled>
            </div>

            <div class="form-group">
                <label>Author</label>
                <input name="author" class="form-control" id="exampleInputPassword1" value="{{$book->author}}" disabled>
            </div>

            <div class="form-group">
                <label>Quantity</label>
                <input name="author" class="form-control" value="{{$book->quantity}}" disabled>
                <small>You can only issue one book</small>
            </div>

            <button type="submit" class="btn btn-primary mb-3">Confirm</button><br/>
        </div>
    </form>
@endsection
