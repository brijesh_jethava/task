@extends('layout.index')

@section('content')

    <div class="container border shadow-lg mt-5 p-5 bg-light">

        @include('admin.auth.messages')
        <table class="table bg-light" id="myTable">
            <thead>
            <tr>
                <th scope="col">Sr no.</th>
                <th scope="col">Name of book</th>
                <th scope="col">Category</th>
                <th scope="col">Name of author</th>
                <th scope="col">Quantity</th>
                <th scope="col"></th>
            </tr>
            </thead>
            <tbody>
            @php
                $i = 1;
            @endphp
            @foreach($book as $book)
                <tr>

                    <th scope="row">{{$i++}}</th>
                    <td>{{$book->book_name}}</td>
                    <td>{{$book->category->category}}</td>
                    <td>{{$book->author}}</td>
                    <td>{{$book->quantity}}</td>
                    @if($book->quantity != 0)
                    <td>
                            <a href="{{route('issue:book' , $book->id)}}" class="btn btn-primary">Issue</a>
                    </td>
                    @else
                        <td>
                            <input type="button" class="btn btn-primary" disabled value="Issue">
                        </td>
                    @endif
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

@endsection

