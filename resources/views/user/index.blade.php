<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link href="/css/app.css" rel="stylesheet">
</head>
<body>

@guest()
    <div class="text-center" style="margin-top: 20%">
        <h2>Welcome</h2>
        <a style="font-size: 20px;text-decoration: none" href="{{route('user:login')}}">Login</a>
    </div>
@endguest

@auth('web')
    @include('layout.index')
@endauth



</body>
</html>


