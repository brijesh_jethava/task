@extends('layout.index')

@section('content')

    <div class="container border shadow-lg mt-5 p-5 bg-light">

        @include('admin.auth.messages')
        <table class="table bg-light" id="myTable">
            <thead>
            <tr>
                <th scope="col">Sr no.</th>
                <th scope="col">Name of book</th>
                <th scope="col">Category</th>
                <th scope="col">Name of author</th>
                <th scope="col">Quantity</th>
                <th scope="col">Date of issue</th>
                <th scope="col">Date of Return</th>
                <th scope="col"></th>

            </tr>
            </thead>
            <tbody>
            @php
                $i = 1;
            @endphp
            @foreach($issuedbook as $issuedbook)
                <tr>
                    <th scope="row">{{$i++}}</th>
                    <td>{{$issuedbook->book->book_name}}</td>
                    <td>{{$issuedbook->book->category->category}}</td>
                    <td>{{$issuedbook->book->author}}</td>
                    <td>{{$issuedbook->quantity}}</td>
                    <td>{{$issuedbook->created_at->toDateString()}}</td>
                    <td style="color: {{now()->toDateString() >= $issuedbook->created_at->addDays(13)->toDateString() ? 'red' : ''}};font-weight:{{now()->toDateString() >= $issuedbook->created_at->addDays(13)->toDateString() ? 'bolder' : ''}} ">{{$issuedbook->return_date}}</td>
                    <td><a href="{{route('return:book' , $issuedbook->id)}}" class="btn btn-primary">Return</a></td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
<p style="color: red;margin-left: 100px;font-size: 20px" class="mt-2 text-bold"><span style="font-weight: bold;font-size: large">* </span>You have to return your issued book within fifteen days</p>
@endsection
