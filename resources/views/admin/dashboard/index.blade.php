@extends('admin.layout.index')

@section('title')
    Dashboard
@stop



@section('content')
    <div class="page-title text-center">
        <h3 class="mt-0">Dashboard</h3>
    </div>

    <div class="row text-center">
        <div class="col-md-12">
            <div class="grid simple">
                <div class="grid-body no-border">
                    <h2>Hello {{ auth('admin')->user()->username }}</h2>
                </div>
            </div>
        </div>
    </div>
@stop
