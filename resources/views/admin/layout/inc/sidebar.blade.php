<div>
    <a class="{{ request()->is('admin/dashboard') ? 'active' : '' }}" href="{{ route('dashboard.index') }}"><i class="fa fa-home pr-2" ></i>Dashboard</a>
    <a class="{{ request()->is('admin/customer*') ? 'active' : '' }}" href="{{ route('customer.index') }}"><i class="fa fa-user pr-2" ></i>User</a>
    <a class="{{ request()->is('admin/book*') ? 'active' : '' }}" href="{{ route('book.index') }}"><i class="fa fa-book pr-2" ></i>Books</a>
    <a class="{{ request()->is('admin/issuedbook*') ? 'active' : '' }}" href="{{ route('issued:book') }}"><i class="fa fa-folder-open pr-2" ></i>Isuued book</a>
    <a class="{{ request()->is('admin/category*') ? 'active' : '' }}" href="{{ route('category.index') }}"><i class="fa fa-list pr-2" ></i>Categories</a>
</div>
