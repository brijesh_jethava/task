{{--<a href="{{ route('admin:logout') }}">--}}
{{--    <i class="fa fa-power-off pr-3 mt-4" style="font-size: 25px"></i>--}}
{{--</a>--}}
<ul class="navbar-nav ml-auto ml-md-0">
    <li  class="nav-item dropdown">
        <a href="#" class="dropdown-toggle" style="font-size: 25px;color: #2a2a2a" data-toggle="dropdown"><i class="fa fa-user mt-3"></i></a>
        <div class="dropdown-menu dropdown-menu-right p-3" aria-labelledby="userDropdown">
            <a class="dropdown-item" style="font-size: 19px" href="{{ route('admin:logout') }}"><i class="fa fa-sign-out mr-2"></i>Logout</a>
        </div>
    </li>
</ul>
