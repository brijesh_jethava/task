<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('title')</title>
    <link href="/css/app.css" rel="stylesheet">
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="/css/custom.css">
</head>
<body>

<div>
    <div class="sidebar bg-dark col-md-2">
        @include('.admin.layout.inc.sidebar')
    </div>

    <div class="col-md-10 float-md-right">

        <div style="height: 50px;margin-left: 95%">
            @include('admin.layout.inc.header')
        </div>

        <div style="background-color: #d6d8d9;" class="mt-4 jumbotron -text-height" >
            @yield('content')
        </div>

    </div>
</div>

<script
    src="https://code.jquery.com/jquery-3.5.1.js"
    integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc="
    crossorigin="anonymous"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="/js/app.js"></script>
<script src="//cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script>
    function myfunction(id)
    {
        $.ajax({
            url: 'approve/' + id,
            type: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },

            success: function (_response)
            {

            },
            error: function (_response) {
                // Handle error
            }
        });
    }
</script>
<script>

    $('.approve').click(function(){
        var $this = $(this);

        if($this.hasClass('approve')){
            alert("User approved successfully")
            $this.text('Approved');
        } else {
            $this.text('Approve');
        }
    });
</script>
<script>
    $(document).ready( function () {
        $('#myTable').DataTable();
    } );
</script>
</body>
</html>
