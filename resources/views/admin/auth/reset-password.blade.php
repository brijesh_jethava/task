@extends('admin.auth.layout')

@section('title')
    Reset password
@endsection

@section('content')

    <form action="{{ route('post:admin:reset:password',$user->id) }}" method="post">
        @csrf

        @include('admin.auth.messages')
        <div  style="margin-top: 10%" class="container shadow border-dark py-4 px-4 col-md-4 col-sm-12 ">
            <div class="form-group" style="">
                <label for="exampleInputEmail1">Password</label>
                <input type="Password" name="password" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="*******">
                @error('password')
                <p style="color: red">{{$message}}</p>
                @enderror

            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Confirm Password</label>
                <input type="password" name="confirm_password" class="form-control" id="exampleInputPassword1" placeholder="*******">
                @error('confirm_password')
                <p style="color: red">{{$message}}</p>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary mb-2">Login</button><br/>
            <button class="btn btn-primary>Reset Password</button>
        </div>
    </form>

@endsection
