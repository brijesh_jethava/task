@extends('admin.auth.layout')

@section('title')
    Forgot password
@endsection

@section('content')

    <form action="{{ route('post:admin:forgot:password') }}" method="post">
        @csrf

        @include('admin.auth.messages')
        <div  style="margin-top: 10%" class="container shadow border-dark py-4 px-4 col-md-4 col-sm-12 ">
            <div class="form-group" style="">
                <label for="exampleInputEmail1">Email address</label>
                <input type="email" name="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
                @error('email')
                <p style="color: red">{{$message}}</p>
                @enderror

            </div>

            <button type="submit" class="btn btn-primary mb-2">Send Email</button><br/>
        </div>
    </form>

@endsection
