@extends('admin.auth.layout')

@section('title')
        Admin|Login
@endsection

@section('content')
    <form action="{{ route('post:admin:login') }}" method="post">
        @csrf

        @include('admin.auth.messages')

        <div  style="margin-top: 10%" class="container shadow border-dark py-4 px-4 col-md-4 col-sm-12 ">
            <div class="form-group" style="">
                <label for="exampleInputEmail1">Email address</label>
                <input type="email" name="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
                @error('email')
                <p style="color: red">{{$message}}</p>
                @enderror

            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Password</label>
                <input type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
                @error('password')
                <p style="color: red">{{$message}}</p>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary mb-2">Login</button><br/>
            <a href="{{ route('admin:forgot:password') }}">Forgot Password?</a>
        </div>
    </form>
@endsection

