@extends('admin.layout.index')

@section('content')

    <form action="{{route('category.store')}}" method="post">
        @csrf
        @include('admin.auth.messages')

        <div  style="margin-top: 10%" class="container shadow bg-light mb-5 border-dark py-4 px-4 col-md-4 col-sm-12 ">
            <div class="form-group" style="">
                <label>Name of category</label>
                <input type="text" name="category" class="form-control" id="book" placeholder="Enter name of category">
                @error('category')
                <p style="color: red">{{$message}}</p>
                @enderror
            </div>

            <button type="submit" class="btn btn-primary mb-2">Add</button>
            <a href="{{route('category.index')}}" type="submit" class="btn btn-primary mb-2">Cancel</a>
        </div>
    </form>

@endsection
