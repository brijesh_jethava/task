@extends('admin.layout.index')

@section('content')

    <form action="{{route('category.update' , $category->id)}}" method="post">
        @csrf
        @method('PUT')
        @include('admin.auth.messages')

        <div  style="margin-top: 10%" class="container shadow mb-5 bg-light border-dark py-4 px-4 col-md-4 col-sm-12 ">
            <div class="form-group" style="">
                <label>Name of category</label>
                <input type="text" name="category" class="form-control" id="book" placeholder="Enter name of category" value="{{$category->category}}">
                @error('category')
                <p style="color: red">{{$message}}</p>
                @enderror
            </div>

            <button type="submit" class="btn btn-primary mb-2">Update</button>
            <a href="{{route('category.index')}}" type="submit" class="btn btn-primary mb-2">Cancel</a>
        </div>
    </form>

@endsection
