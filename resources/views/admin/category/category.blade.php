@extends('admin.layout.index')

@section('content')

    <div>
        <a href="{{route('category.create')}}" class="btn btn-primary" style="margin-left: 82%;">Add new category</a>
    </div>

    <div class="container border bg-light shadow-lg mt-5 p-5">

        @include('admin.auth.messages')
        <table class="table" id="myTable">
            <thead>
            <tr>
                <th scope="col">Sr no.</th>
                <th scope="col">Category</th>
                <th scope="col"></th>

            </tr>
            </thead>
            <tbody>
            @php
                $i = 1;
            @endphp
            @foreach($category as $category)
                <tr>
                    <th scope="row">{{$i++}}</th>
                    <td>{{$category->category}}</td>
                    <td><a href="category/{{$category->id}}/edit" class="btn"><i class="fa fa-edit" style="font-size: 25px;color: #3d8fd1"></i></a></td>
                </tr>
            @endforeach
            </tbody>
        </table>

    </div>
    <a href="{{route('dashboard.index')}}" class="btn btn-primary" style="margin-top: 2%; margin-left: 50%">Back</a>
@endsection
