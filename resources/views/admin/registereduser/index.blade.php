@extends('admin.layout.index')

@section('content')
    <div class="container border shadow-lg mt-5 p-5 bg-light">
        @include('admin.auth.messages')
    <table class="table" id="myTable">
        <thead>
        <tr>
            <th scope="col">Sr no.</th>
            <th scope="col">name</th>
            <th scope="col">email</th>
            <th scope="col">Action</th>
        </tr>
        </thead>
        <tbody>
        @php
         $i = 1;
        @endphp
        @foreach($result as $result)
        <tr>

            <th scope="row">{{$i++}}</th>
            <td>{{$result->name}}</td>
            <td>{{$result->email}}</td>
            <td>
                    <meta name="csrf-token" content="{{ csrf_token() }}">
                    <button class="btn btn-primary approve" id="{{$result->id}}" onclick="myfunction({{$result->id}})">
                    {{($result->is_approve)==0 ? 'Approve' : 'Approved'}}
                    </button>
            </td>

        </tr>
        @endforeach
        </tbody>
    </table>
    </div>

    <a href="{{route('dashboard.index')}}" class="btn btn-primary" style="margin-top: 5%; margin-left: 50%">Back</a>
@endsection

