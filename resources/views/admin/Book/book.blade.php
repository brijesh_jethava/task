@extends('admin.layout.index')

@section('content')
    <div>
    <a href="{{route('book.create')}}" class="btn btn-primary" style="margin-left: 84%;">Add new book</a>
    </div>
        <div class="container border shadow-lg mt-5 p-5 bg-light">

        @include('admin.auth.messages')
        <table class="table bg-light" id="myTable">
            <thead>
            <tr>
                <th scope="col">Sr no.</th>
                <th scope="col">Name of book</th>
                <th scope="col">Category</th>
                <th scope="col">Name of author</th>
                <th scope="col">Quantity</th>
                <th scope="col"></th>
                <th scope="col"></th>
            </tr>
            </thead>
            <tbody>
            @php
                $i = 1;
            @endphp
            @foreach($book as $book)
                <tr>

                    <th scope="row">{{$i++}}</th>
                    <td>{{$book->book_name}}</td>
                    <td>{{$book->category->category}}</td>
                    <td>{{$book->author}}</td>
                    <td>{{$book->quantity}}</td>
                    <td><a href="book/{{$book->id}}/edit" class="btn"><i class="fa fa-edit" style="font-size: 25px;color: #3d8fd1"></i></a></td>
                    <td>
                        <form action="{{ route('book.destroy', $book->id) }}" method="POST">
                            @method('DELETE')
                            @csrf
                            <button type="submit" class="btn"><i class="fa fa-trash" style="font-size: 25px;color: red"></i></button>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>

    </div>
    <a href="{{route('dashboard.index')}}" class="btn btn-primary" style="margin-top: 5%; margin-left: 50%">Back</a>
@endsection
