@extends('admin.layout.index')

@section('content')

    <form action="{{route('book.store')}}" method="post">
        @csrf
        @include('admin.auth.messages')

        <div class="container bg-light shadow border-dark py-4 px-4 col-md-4 col-sm-12 ">
            <div class="form-group" style="">
                <label>Name of book</label>
                <input type="text" name="book_name" class="form-control" id="book" placeholder="Enter name of book">
                @error('book')
                <p style="color: red">{{$message}}</p>
                @enderror
            </div>

            <div class="form-group">
                <label>Author of book</label>
                <input type="text" name="author" class="form-control" id="author" placeholder="Enter name of author">
                @error('author')
                <p style="color: red">{{$message}}</p>
                @enderror
            </div>

            <div class="form-group">
                <label>Quantity</label>
                <input type="number" name="quantity" class="form-control" id="quantity" placeholder="Enter quantity">
                @error('quantity')
                <p style="color: red">{{$message}}</p>
                @enderror
            </div>

            <div class="form-group">
               <tr>
                    <td>
                        <label>Category</label>
                    </td>

                   <td>:</td>

                   <td>
                       <select name="category_id">
                           @foreach($category as $category)
                               <option value="{{$category->id}}">
                                   {{$category->category}}
                               </option>
                           @endforeach
                       </select>
                   </td>
                @error('category')
                <p style="color: red">{{$message}}</p>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary mb-2">Add</button>
            <a href="{{route('book.index')}}" type="submit" class="btn btn-primary mb-2">Cancel</a>
        </div>
    </form>

@endsection
