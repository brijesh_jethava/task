@extends('admin.layout.index')

@section('content')

    <form action="{{route('book.update', $book->id)}}" method="post">
        @csrf
        @method('PUT')
        @include('admin.auth.messages')

        <div class="container shadow bg-light border-dark py-4 px-4 col-md-4 col-sm-12 ">
            <div class="form-group" style="">
                <label>Name of book</label>
                <input type="text" name="book_name" class="form-control" id="book" placeholder="Enter name of book" value="{{$book->book_name}}">
                @error('book')
                <p style="color: red">{{$message}}</p>
                @enderror
            </div>

            <div class="form-group">
                <label>Author of book</label>
                <input type="text" name="author" class="form-control" id="author" placeholder="Enter name of author" value="{{$book->author}}">
                @error('author')
                <p style="color: red">{{$message}}</p>
                @enderror
            </div>

            <div class="form-group">
                <label>Quantity</label>
                <input type="number" name="quantity" class="form-control" id="quantity" placeholder="Enter quantity" value="{{$book->quantity}}">
                @error('quantity')
                <p style="color: red">{{$message}}</p>
                @enderror
            </div>

            <div class="form-group">
                <tr>
                    <td>
                        <label>Category</label>
                    </td>

                    <td>:</td>

                    <td>
                        <select name="category_id">
                            @foreach($category as $category)
                                <option value="{{$category->id}}">
                                    {{$category->category}}
                                </option>
                            @endforeach
                        </select>
                    </td>
                    @error('category')
                    <p style="color: red">{{$message}}</p>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary mb-2">Update</button>
            <a href="{{route('book.index')}}" class="btn btn-primary mb-2">Cancel</a><br/>
        </div>
    </form>

@endsection

