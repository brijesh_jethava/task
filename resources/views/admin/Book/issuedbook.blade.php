@extends('admin.layout.index')

@section('content')

    <div class="container border shadow-lg mt-5 p-5 bg-light">

        @include('admin.auth.messages')
        <table class="table bg-light" id="myTable">
            <thead>
            <tr>
                <th scope="col">Sr no.</th>
                <th scope="col">Name of book</th>
                <th scope="col">Category</th>
                <th scope="col">Name of author</th>
                <th scope="col">Quantity</th>
                <th scope="col">Issued by</th>
                <th scope="col">Date of issue</th>

            </tr>
            </thead>
            <tbody>
            @php
                $i = 1;
            @endphp
            @foreach($issuedbook as $issuedbook)
                <tr>
                    <th scope="row">{{$i++}}</th>
                    <td>{{$issuedbook->book->book_name}}</td>
                    <td>{{$issuedbook->book->category->category}}</td>
                    <td>{{$issuedbook->book->author}}</td>
                    <td>{{$issuedbook->quantity}}</td>
                    <td>{{$issuedbook->user->name}}</td>
                    <td>{{$issuedbook->created_at->toDateString()}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

@endsection
