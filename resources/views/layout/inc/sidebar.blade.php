<div>
    <a href="{{route('user:home')}}" class="{{ request()->is('home') ? 'active' : '' }}"><i class="fa fa-home pr-2" ></i>Home</a>
    <a href="{{route('show:book')}}" class="{{ request()->is('book*') ? 'active' : '' }}"><i class="fa fa-book pr-2" ></i>Book</a>
    <a href="{{route('user:issued:book', auth()->id())}}" class="{{ request()->is('mybook*') ? 'active' : '' }}"><i class="fa fa-folder-open pr-2" ></i>My books</a>
</div>
